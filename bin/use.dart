bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool everyUserOver13(Iterable<User> users) {
  return users.every((user) => user.age > 13);
}

Iterable<User> filterOutUnder21(Iterable<User> users) {
  return users.where((users) => users.age >= 21);
}

Iterable<User> findShortNamed(Iterable<User> users) {
  return users.where((users) => users.name.length <= 3);
}

Iterable<String> getNameAndAge(Iterable<User> users) {
  return users.map((users) => '${users.name} is ${users.age}');
}

void main() {
  var users = [
    User(name: 'ABCDE', age: 14),
    User(name: 'BCDEF', age: 18),
    User(name: 'CDE', age: 18),
    User(name: 'DEF', age: 21),
    User(name: 'E', age: 25),
  ];
  if (anyUserUnder18(users)) {
    print('Have any user under 18');
  }
  if (everyUserOver13(users)) {
    print('every user is over 13');
  }

  var ageMoreThan21User = filterOutUnder21(users);
  print('Age more than 21:');
  for (var user in ageMoreThan21User) {
    print(user.toString());
  }

  var shortNameUser = findShortNamed(users);
  print('Short name less 3:');
  for (var user in shortNameUser) {
    print(user.toString());
  }

  var nameAndAge = getNameAndAge(users);
  for (var user in nameAndAge) {
    print(user);
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});

  @override
  String toString() {
    return '$name, $age';
  }
}
